# Advanced Sample Hardhat Project

This project demonstrates an advanced Hardhat use case, integrating other tools commonly used alongside Hardhat in the ecosystem.

The project comes with a sample contract, a test for that contract, a sample script that deploys that contract, and an example of a task implementation, which simply lists the available accounts. It also comes with a variety of other tools, preconfigured to work with the project code.

Try running some of the following tasks:

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
npx hardhat help
REPORT_GAS=true npx hardhat test
npx hardhat coverage
npx hardhat run scripts/deploy.ts
TS_NODE_FILES=true npx ts-node scripts/deploy.ts
npx eslint '**/*.{js,ts}'
npx eslint '**/*.{js,ts}' --fix
npx prettier '**/*.{json,sol,md}' --check
npx prettier '**/*.{json,sol,md}' --write
npx solhint 'contracts/**/*.sol'
npx solhint 'contracts/**/*.sol' --fix
```

ERC721 Token here [0x8381b4D1EfF8a27aF6d051363A1b1a370331fa1B](https://rinkeby.etherscan.io/address/0x8381b4D1EfF8a27aF6d051363A1b1a370331fa1B)
OpenSea collection for this ERC721 [is here](https://testnets.opensea.io/collection/cattoken-tewucmarwv)

ERC1155 Token here [0x66f0f8D6F30AF6f5d5E3500Dad8fCB6dB0f20DBB](https://rinkeby.etherscan.io/address/0x66f0f8D6F30AF6f5d5E3500Dad8fCB6dB0f20DBB)
OpenSea collection for this ERC1155 [is here](https://testnets.opensea.io/collection/unidentified-contract-ri1mbhyu2f)

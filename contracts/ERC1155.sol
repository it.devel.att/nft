pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "./RoleControl.sol";

contract GameItem is ERC1155, RoleControl {
    uint256 public constant GOLD = 0;
    uint256 public constant SILVER = 1;
    uint256 public constant COMMON_SWORD = 2;
    uint256 public constant COMMON_SCHIELD = 3;
    uint256 public constant THORS_HAMMER = 4;

    constructor() ERC1155("") RoleControl(msg.sender) {
        _mint(msg.sender, GOLD, 10**6, "");
        _mint(msg.sender, SILVER, 10**12, "");
        _mint(msg.sender, COMMON_SCHIELD, 10**6, "");
        _mint(msg.sender, COMMON_SWORD, 10**6, "");
        _mint(msg.sender, THORS_HAMMER, 1, "");
    }

    function mint(address to, uint256 id, uint256 amount, bytes memory data) external onlyMinter {
        _mint(to, id, amount, data);
    }

    function mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data) external onlyMinter {
        _mintBatch(to, ids, amounts, data);
    }

    function setURI(string memory newURI) external onlyAdmin {
        _setURI(newURI);
    }

     // The following functions are overrides required by Solidity.

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC1155, AccessControl)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
} 

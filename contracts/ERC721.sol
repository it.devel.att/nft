//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "./RoleControl.sol";

contract CatToken is ERC721, ERC721URIStorage, RoleControl {
    using Counters for Counters.Counter;

    Counters.Counter private _tokenIdCounter;
    string private _baseURIValue;

    constructor() ERC721("CatToken", "CTN") RoleControl(msg.sender) {}

    function mint(address to, string memory uri) public onlyMinter {
        _tokenIdCounter.increment();
        uint256 tokenId = _tokenIdCounter.current();
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, uri);
    }

    function setBaseURI(string memory baseURI) external onlyAdmin {
        _baseURIValue = baseURI;
    }

    function getBaseURI() external view returns(string memory) {
        return _baseURIValue;
    }

    function _baseURI() internal view override(ERC721) returns (string memory) {
        return _baseURIValue;
    }

     // The following functions are overrides required by Solidity.

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, AccessControl)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
} 

//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/AccessControl.sol";

contract RoleControl is AccessControl {
    bytes32 public constant MINTER_ROLE = keccak256("MINTER");

    constructor(address root) {
        _setupRole(DEFAULT_ADMIN_ROLE, root);

        _setRoleAdmin(MINTER_ROLE, DEFAULT_ADMIN_ROLE);
        addMinter(root);
    }

    function isAdmin(address account) public virtual view returns(bool) {
        return hasRole(DEFAULT_ADMIN_ROLE, account);
    }

    modifier onlyAdmin() {
        require(isAdmin(msg.sender), "Only Admin access.");
        _;
    }

    function addAdmin(address account) public virtual onlyAdmin {
        grantRole(DEFAULT_ADMIN_ROLE, account);
    }

    function isMinter(address account) public virtual view returns(bool) {
        return hasRole(MINTER_ROLE, account);
    }

    modifier onlyMinter() {
        require(isMinter(msg.sender), "Only Minter access.");
        _;
    }

    function addMinter(address account) public virtual onlyAdmin {
        grantRole(MINTER_ROLE, account);
    }
}

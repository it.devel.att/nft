import { ethers } from "hardhat";

async function main() {
  const Token = await ethers.getContractFactory("GameItem");
  const ERC1155Token = await Token.deploy();

  await ERC1155Token.deployed();

  console.log("ERC1155 Token deployed to:", ERC1155Token.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});

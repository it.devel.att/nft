import { ethers } from "hardhat";

async function main() {
  const Token = await ethers.getContractFactory("CatToken");
  const ERC721Token = await Token.deploy();

  await ERC721Token.deployed();

  console.log("ERC721 Token deployed to:", ERC721Token.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});

import { task } from "hardhat/config";

async function getContract(hre: any, contractAddress: string) {
  const Token = await hre.ethers.getContractFactory("CatToken");
  const contract = await Token.attach(contractAddress);
  return contract;
}

task("mint", "Mint new ERC721 token")
  .addParam("contract", "Address of ERC721 contract")
  .addParam("to", "Address to who mint this token")
  .addParam("uri", "URI of new token")
  .setAction(async (taskArgs, hre) => {
    const token = await getContract(hre, taskArgs.contract);
    await token.mint(taskArgs.to, taskArgs.uri);
    console.log(`Success mint new token`);
  });

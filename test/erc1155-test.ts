import { expect } from "chai";
import { ethers, network } from "hardhat";
import { Contract } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

describe("ERC1155", function () {
  const zeroAddress: string = '0x0000000000000000000000000000000000000000';
  const zeroBytes = '0x';
  let ERC1155Token: Contract;

  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addr3: SignerWithAddress;
  let addrs: SignerWithAddress[];

  const GOLD = 0;
  const SILVER = 1;
  const COMMON_SWORD = 2;
  const COMMON_SCHIELD = 3;
  const THORS_HAMMER = 4;

  const baseURI = "http://some-site/{id}.json";

  let clean: any;

  before(async () => {
    const Token = await ethers.getContractFactory("GameItem");
    ERC1155Token = await Token.deploy();

    [owner, addr1, addr2, addr3, ...addrs] = await ethers.getSigners();

    await ERC1155Token.setURI(baseURI);
    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  afterEach(async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [clean],
    });
    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  async function mintTo(address: string, tokenID: number, count: number) {
    await ERC1155Token.mint(address, tokenID, count, zeroBytes);
  }

  it("Owner should mint to address", async function () {
    await ERC1155Token.mint(addr1.address, GOLD, 100, zeroBytes);
    expect((await ERC1155Token.balanceOf(addr1.address, GOLD)).toString()).to.eq("100");
  });

  it("Owner should batch mint to address", async function () {
    await ERC1155Token.mintBatch(addr1.address, [GOLD, SILVER], [100, 200], zeroBytes);
    expect((await ERC1155Token.balanceOf(addr1.address, GOLD)).toString()).to.eq("100");
    expect((await ERC1155Token.balanceOf(addr1.address, SILVER)).toString()).to.eq("200");
  });

  it("Only minter can mint", async function () {
     await expect(ERC1155Token.connect(addr1).mint(addr1.address, GOLD, 100, zeroBytes)).to.be.revertedWith("Only Minter access.");
     await expect(ERC1155Token.connect(addr1).mintBatch(addr1.address, [GOLD, SILVER], [100, 200], zeroBytes)).to.be.revertedWith("Only Minter access.");
  });

  it("Should show balance of tokens", async function () {
    expect((await ERC1155Token.balanceOf(addr1.address, GOLD)).toString()).to.eq("0");
    await mintTo(addr1.address, GOLD, 3);
    expect((await ERC1155Token.balanceOf(addr1.address, GOLD)).toString()).to.eq("3");
  });

  it("Should show balance of batch tokens", async function () {
    await mintTo(addr1.address, GOLD, 100);
    await mintTo(addr2.address, SILVER, 200);
    await mintTo(addr3.address, COMMON_SWORD, 300);
    const balances = await ERC1155Token.balanceOfBatch([addr1.address, addr2.address, addr3.address], [GOLD, SILVER, COMMON_SWORD]);
    expect(balances.length).to.eq(3);
    expect(balances[0].toString()).to.eq("100");
    expect(balances[1].toString()).to.eq("200");
    expect(balances[2].toString()).to.eq("300");
  });

  it("Show token uri", async function () {
    const token1URI = await ERC1155Token.uri(GOLD);
    expect(token1URI).to.eq(baseURI);
  });

  it("Should setApprovalForAll", async function () {
    expect(await ERC1155Token.isApprovedForAll(owner.address, addr1.address)).to.eq(false);
    await ERC1155Token.setApprovalForAll(addr1.address, true);
    expect(await ERC1155Token.isApprovedForAll(owner.address, addr1.address)).to.eq(true);
  });

  it("Should safe transfer token from owner to address", async function () {
    expect((await ERC1155Token.balanceOf(addr1.address, GOLD)).toString()).to.eq("0");
    expect((await ERC1155Token.balanceOf(owner.address, GOLD)).toString()).to.eq("1000000");

    await ERC1155Token.safeTransferFrom(owner.address, addr1.address, GOLD, 100, zeroBytes);

    expect((await ERC1155Token.balanceOf(addr1.address, GOLD)).toString()).to.eq("100");
    expect((await ERC1155Token.balanceOf(owner.address, GOLD)).toString()).to.eq("999900");
  });

  it("Should safe batch transfer token from owner to address", async function () {
    expect((await ERC1155Token.balanceOf(addr1.address, GOLD)).toString()).to.eq("0");
    expect((await ERC1155Token.balanceOf(addr1.address, SILVER)).toString()).to.eq("0");
    expect((await ERC1155Token.balanceOf(owner.address, GOLD)).toString()).to.eq("1000000");
    expect((await ERC1155Token.balanceOf(owner.address, SILVER)).toString()).to.eq("1000000000000");

    await ERC1155Token.safeBatchTransferFrom(owner.address, addr1.address, [GOLD, SILVER], [100, 200], zeroBytes);

    expect((await ERC1155Token.balanceOf(addr1.address, GOLD)).toString()).to.eq("100");
    expect((await ERC1155Token.balanceOf(addr1.address, SILVER)).toString()).to.eq("200");
    expect((await ERC1155Token.balanceOf(owner.address, GOLD)).toString()).to.eq("999900");
    expect((await ERC1155Token.balanceOf(owner.address, SILVER)).toString()).to.eq("999999999800");
  });
});

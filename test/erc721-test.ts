import { expect } from "chai";
import { ethers, network } from "hardhat";
import { Contract } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

describe("ERC721", function () {
  const zeroAddress: string = '0x0000000000000000000000000000000000000000';
  let ERC721Token: Contract;

  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addrs: SignerWithAddress[];

  let clean: any;
  const defaultURI: string = "http://default.uri.com/";

  before(async () => {
    const Token = await ethers.getContractFactory("CatToken");
    ERC721Token = await Token.deploy();

    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  afterEach(async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [clean],
    });
    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  async function mintTo(address: string, count: number) {
    for (let i = 0; i < count; i++) {
      await ERC721Token.mint(address, defaultURI);
    }
  }

  it("Owner should mint to address", async function () {
    await ERC721Token.mint(addr1.address, defaultURI);
    expect(await ERC721Token.ownerOf(1)).to.eq(addr1.address);
  });

  it("Owner grant mint role", async function () {
    await expect(ERC721Token.connect(addr1).mint(addr1.address, defaultURI)).to.be.revertedWith("Only Minter access.");

    await ERC721Token.addMinter(addr1.address);
    await ERC721Token.connect(addr1).mint(addr1.address, defaultURI);
  });

  it("Only minter can mint", async function () {
    await expect(ERC721Token.connect(addr1).mint(addr1.address, defaultURI)).to.be.revertedWith("Only Minter access.");
  });

  it("Should show balance of tokens", async function () {
    expect((await ERC721Token.balanceOf(addr1.address)).toString()).to.eq("0");
    await mintTo(addr1.address, 3);
    expect((await ERC721Token.balanceOf(addr1.address)).toString()).to.eq("3");
  });

  it("Get and set base uri", async function () {
    const baseURI = await ERC721Token.getBaseURI();
    expect(baseURI).to.eq("");

    const newBaseURI = "ipfs://";
    await ERC721Token.setBaseURI(newBaseURI);
    const actualBaseURI = await ERC721Token.getBaseURI();
    expect(actualBaseURI).to.eq(newBaseURI);
  });

  it("Use new base uri as prefix for token uri", async function () {
    const newBaseURI = "ipfs://";
    await ERC721Token.setBaseURI(newBaseURI);
    const tokenURI = "someHashForExample";

    await ERC721Token.mint(addr1.address, tokenURI);
    const token1URI = await ERC721Token.tokenURI(1);
    expect(token1URI).to.eq(newBaseURI + tokenURI);
  });

  it("Should approve token to address", async function () {
    await mintTo(addr1.address, 3);
    await ERC721Token.connect(addr1).approve(addr2.address, 1);

    expect(await ERC721Token.getApproved(1)).to.eq(addr2.address);
  });

  it("Shouldn't approve not owned token", async function () {
    await mintTo(addr1.address, 3);
    await expect(ERC721Token.approve(addr2.address, 1)).to.be.revertedWith("ERC721: approve caller is not owner nor approved for all");
  });

  it("Should transfer token from owner to address", async function () {
    await mintTo(addr1.address, 3);

    expect((await ERC721Token.balanceOf(addr1.address)).toString()).to.eq("3");
    expect((await ERC721Token.balanceOf(addr2.address)).toString()).to.eq("0");
    expect(await ERC721Token.ownerOf(1)).to.eq(addr1.address);

    await ERC721Token.connect(addr1).transferFrom(addr1.address, addr2.address, 1);

    expect((await ERC721Token.balanceOf(addr1.address)).toString()).to.eq("2");
    expect((await ERC721Token.balanceOf(addr2.address)).toString()).to.eq("1");
    expect(await ERC721Token.ownerOf(1)).to.eq(addr2.address);
  });

  it("Shouldn't transfer token from not approved address", async function () {
    await mintTo(addr1.address, 3);

    await expect(ERC721Token.transferFrom(owner.address, addr2.address, 1)).to.be.revertedWith("ERC721: transfer caller is not owner nor approved");
  });

  it("Should safe transfer token from owner to address", async function () {
    await mintTo(addr1.address, 3);

    expect((await ERC721Token.balanceOf(addr1.address)).toString()).to.eq("3");
    expect((await ERC721Token.balanceOf(addr2.address)).toString()).to.eq("0");
    expect(await ERC721Token.ownerOf(1)).to.eq(addr1.address);

    await ERC721Token.connect(addr1)["safeTransferFrom(address,address,uint256)"](addr1.address, addr2.address, 1);

    expect((await ERC721Token.balanceOf(addr1.address)).toString()).to.eq("2");
    expect((await ERC721Token.balanceOf(addr2.address)).toString()).to.eq("1");
    expect(await ERC721Token.ownerOf(1)).to.eq(addr2.address);
  });

  it("Shouldn't safe transfer token from not approved address", async function () {
    await mintTo(addr1.address, 3);

    await expect(ERC721Token["safeTransferFrom(address,address,uint256)"](addr1.address, addr2.address, 1)).to.be.revertedWith("ERC721: transfer caller is not owner nor approved");
  });

  it("Should set approval for all tokens to address", async function () {
    await mintTo(addr1.address, 3);

    expect((await ERC721Token.isApprovedForAll(addr1.address, addr2.address))).to.eq(false);
    await ERC721Token.connect(addr1).setApprovalForAll(addr2.address, true);
    expect((await ERC721Token.isApprovedForAll(addr1.address, addr2.address))).to.eq(true);
  });
});
